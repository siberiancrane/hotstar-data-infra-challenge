package main

import (
	"fmt"
	"math/rand"
	"time"
	"net"
)

func main() {
	emitLogs()
}

func emitLogs() {
	randomTimeToSleep := time.Duration(rand.Intn(1000))
	fmt.Println("Launching server...")

	ln, _ := net.Listen("tcp", ":8888")

	conn, _ := ln.Accept()

	for {
		//fmt.Println(NewLogLine())
		conn.Write([]byte(NewLogLine() + "\n"))
		time.Sleep(randomTimeToSleep * time.Millisecond)
	}
}
